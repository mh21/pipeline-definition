#!/bin/bash

for file in "${BASH_SOURCE[0]%/*}"/../pipeline/functions/*.yml; do
    sed '/^    /{s/^    //;b};z' "${file}" > "${file}.sh"
    # shellcheck disable=SC1090  # can't follow non-constant source
    source "${file}.sh"
done
