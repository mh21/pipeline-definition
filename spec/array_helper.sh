# shellcheck shell=bash

shellspec_array_helper_configure() {
  import 'array/subjects'
  import 'array/matchers'
  import 'array/modifiers'
}
