# shellcheck shell=bash

shellspec_syntax 'shellspec_modifier_element'
function shellspec_modifier_element() {
    shellspec_syntax_param count [ $# -ge 1 ] || return 0
    shellspec_syntax_param 1 is number "$1" || return 0
    if [[ -n ${SHELLSPEC_SUBJECT[$1]+x} ]]; then
        SHELLSPEC_SUBJECT=${SHELLSPEC_SUBJECT[$1]}
    else
        unset SHELLSPEC_SUBJECT
    fi
    shift

    shellspec_syntax_dispatch modifier "$@"
}
